package de.kybu.bedwars.countdowns;

import de.kybu.gameframe.game.countdown.Countdown;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.BukkitUtil;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.github.paperspigot.Title;

public class LobbyCountdown extends Countdown {

    public LobbyCountdown() {
        super(60);
    }

    @Override
    public void onFinish() {
        ModuleService.getInstance().getActivatedModule().getGame().setCurrentCountdown(null);
        Bukkit.getOnlinePlayers().forEach(player -> {
            player.setExp(0);
            player.setLevel(0);
            player.playSound(player.getLocation(), Sound.NOTE_PLING, 3, 1);
        });

        BukkitUtil.broadcastTranslatedMessage("game-starts-now", ModuleService.getInstance().getPrefix());
        BukkitUtil.broadcastTranslatedMessage("all-players-teleported", ModuleService.getInstance().getPrefix());
        ModuleService.getInstance().getActivatedModule().getGame().startGame();
    }

    @Override
    public void onTick() {
        if(getCurrentSeconds() == 30 || getCurrentSeconds() == 15 || getCurrentSeconds() == 10 || getCurrentSeconds() <= 5){
            Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.NOTE_BASS, 3, 1));
            BukkitUtil.broadcastTranslatedMessage("game-starts-in", ModuleService.getInstance().getPrefix(), getCurrentSeconds() + "");
        }

        if(getCurrentSeconds() == 5){
            Title announcementTitle = Title.builder().title(ModuleService.getInstance().getColor() + "§l" + ModuleService.getInstance().getActivatedModule().getModuleName()).subtitle(ModuleService.getInstance().getActivatedModule().getGame().getMap().getMapName())
                    .fadeIn(10)
                    .stay(20)
                    .fadeOut(10)
                    .build();

            Bukkit.getOnlinePlayers().forEach(player -> player.sendTitle(announcementTitle));
        }

        Bukkit.getOnlinePlayers().forEach(player -> {
            player.setExp(0);
            player.setLevel(getCurrentSeconds());
        });
    }

}
