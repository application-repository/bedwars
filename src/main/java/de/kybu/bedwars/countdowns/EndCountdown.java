package de.kybu.bedwars.countdowns;

import de.kybu.gameframe.game.countdown.Countdown;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.BukkitUtil;
import org.bukkit.Bukkit;

public class EndCountdown extends Countdown {

    public EndCountdown() {
        super(10);
    }

    @Override
    public void onFinish() {
        ModuleService.getInstance().getActivatedModule().getGame().setCurrentCountdown(null);
        BukkitUtil.broadcastTranslatedMessage("game-resetted", ModuleService.getInstance().getPrefix());
        ModuleService.getInstance().getActivatedModule().getGame().resetGame();
    }

    @Override
    public void onTick() {
        if(getCurrentSeconds() <= 5){
            BukkitUtil.broadcastTranslatedMessage("game-stops-in", ModuleService.getInstance().getPrefix(), getCurrentSeconds() + "");
        }

        Bukkit.getOnlinePlayers().forEach(player -> player.setLevel(getCurrentSeconds()));
    }
}
