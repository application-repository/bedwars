package de.kybu.bedwars.listener.specials;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.stream.Stream;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 07.02.2021
 */
public class PlayerInteractHandler implements Listener {

    @EventHandler
    public void handle(final PlayerInteractEvent event){
        if(event.getAction() != Action.RIGHT_CLICK_AIR)
            return;

        if(event.getItem() == null)
            return;

        if(event.getItem().getType() != Material.BLAZE_ROD)
            return;

        this.removeItem(event.getPlayer().getInventory(), event.getItem().getType(), 1);

        this.createSafePlate(event.getPlayer());
    }

    private void createSafePlate(final Player player){
        BedWarsGame bedWarsGame = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();
        Stream.of(
                setBlock(player, 0, 0),
                setBlock(player, 1, 0),
                setBlock(player, 0, 1),
                setBlock(player, 2, 0),
                setBlock(player, 0, 2),
                setBlock(player, -1, 0),
                setBlock(player, 0, -1),
                setBlock(player, -2, 0),
                setBlock(player, 0, -2),
                setBlock(player, 1, 1),
                setBlock(player, -1, 1),
                setBlock(player, 1, -1),
                setBlock(player, -1, -1)
        ).forEach(o -> bedWarsGame.addBlock(o));
        player.setFallDistance(0);
        player.teleport(player.getLocation().add(0, 2, 0));
    }

    private Block setBlock(final Player player, final int x, final int z){
        final Location blockLocation = player.getLocation().clone().subtract(0, 2, 0);
        blockLocation.add(x, 0, z);

        final Block blockAtLocation = blockLocation.getBlock();
        if(blockAtLocation.getType() != Material.AIR)
            return blockAtLocation;

        blockAtLocation.setType(Material.GLASS);
        return blockAtLocation;
    }

    private void removeItem(final Inventory inventory, final Material material, final int amount){
        final int first = inventory.first(material);
        if(first != -1){
            final ItemStack itemStack = inventory.getItem(first);
            if(itemStack.getAmount() <= amount)
                inventory.clear(first);
            else {
                itemStack.setAmount(itemStack.getAmount() - amount);
                inventory.setItem(first, itemStack);
            }
        }
    }

}
