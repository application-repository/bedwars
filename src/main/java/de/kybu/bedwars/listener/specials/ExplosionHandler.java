package de.kybu.bedwars.listener.specials;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 07.02.2021
 */
public class ExplosionHandler implements Listener {

    @EventHandler
    public void handle(final EntityExplodeEvent event){
        BedWarsGame bedWarsGame = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();
        List<Block> bypassBlocks = new ArrayList<>();
        event.blockList()
                .stream()
                .filter(block -> !bedWarsGame.getPlacedBlocks().contains(block))
                .forEach(bypassBlocks::add);

        event.blockList().removeAll(bypassBlocks);
    }

    @EventHandler
    public void handle(final BlockExplodeEvent event){
        BedWarsGame bedWarsGame = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();
        List<Block> bypassBlocks = new ArrayList<>();
        event.blockList()
                .stream()
                .filter(block -> !bedWarsGame.getPlacedBlocks().contains(block))
                .forEach(bypassBlocks::add);

        event.blockList().removeAll(bypassBlocks);
    }

}
