package de.kybu.bedwars.listener.specials;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 07.02.2021
 */
public class FakeChestHandler implements Listener {

    @EventHandler
    public void handle(final PlayerInteractEvent event){
        final Player player = event.getPlayer();

        if(event.getAction() != Action.RIGHT_CLICK_BLOCK)
            return;

        if(event.getClickedBlock() == null)
            return;

        if(event.getClickedBlock().getType() != Material.CHEST)
            return;

        if(event.getClickedBlock().hasMetadata("fakechest")){
            player.getWorld().createExplosion(event.getClickedBlock().getLocation(), 5, false);
        }
    }

}
