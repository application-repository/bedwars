package de.kybu.bedwars.listener.specials;

import de.kybu.bedwars.BedWars;
import de.kybu.gameframe.teams.Team;
import de.kybu.gameframe.teams.TeamService;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 07.02.2021
 */
public class TNTSheepHandler implements Listener {

    @EventHandler
    public void handle(final PlayerInteractEvent event){
        final Player player = event.getPlayer();

        if(event.getItem() == null)
            return;

        if(event.getItem().getType() != Material.MONSTER_EGG)
            return;

        if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){
            this.removeItem(player.getInventory(), Material.MONSTER_EGG, 1);
            final Team team = TeamService.getInstance().getPlayersTeam(player);

            Sheep sheep = player.getWorld().spawn(player.getLocation(), Sheep.class);
            sheep.setTarget(player);
            sheep.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 4, false, false));

            TNTPrimed primed = player.getWorld().spawn(player.getLocation(), TNTPrimed.class);
            primed.setFuseTicks(400);
            Bukkit.getScheduler().runTaskLater(BedWars.getInstance(), () -> {
                sheep.getWorld().createExplosion(sheep.getLocation(), 5, false);
                sheep.setHealth(0);
            }, 400);
            sheep.setPassenger(primed);
        }
    }

    private void removeItem(final Inventory inventory, final Material material, final int amount){
        final int first = inventory.first(material);
        if(first != -1){
            final ItemStack itemStack = inventory.getItem(first);
            if(itemStack.getAmount() <= amount)
                inventory.clear(first);
            else {
                itemStack.setAmount(itemStack.getAmount() - amount);
                inventory.setItem(first, itemStack);
            }
        }
    }

}
