package de.kybu.bedwars.listener;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class BlockedHandlers implements Listener {

    @EventHandler
    public void handle(final CraftItemEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void handle(final HangingBreakEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void handle(final PlayerBedEnterEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void handle(final PlayerInteractEvent event){

        if(!((BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame()).isPlayer(event.getPlayer().getUniqueId())){
            event.setCancelled(true);
            return;
        }

        if(event.getClickedBlock() == null)
            return;

        if(event.getAction() == Action.PHYSICAL){
            if(event.getClickedBlock().getType() == Material.SOIL){
                event.setCancelled(true);
            }
        } else if(event.getAction() == Action.RIGHT_CLICK_BLOCK){
            if(event.getClickedBlock().getType() == Material.BED_BLOCK || event.getClickedBlock().getType() == Material.WORKBENCH
            || event.getClickedBlock().getType() == Material.ANVIL || event.getClickedBlock().getType() == Material.DISPENSER)
                event.setCancelled(true);
        }

    }

    @EventHandler
    public void handle(final PlayerDropItemEvent event){
        event.setCancelled(!((BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame()).isPlayer(event.getPlayer().getUniqueId()));
    }

        @EventHandler
    public void handle(final InventoryOpenEvent event){
        if(event.getView().getType() == InventoryType.MERCHANT)
            event.setCancelled(true);
    }

}
