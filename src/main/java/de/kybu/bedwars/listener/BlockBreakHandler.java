package de.kybu.bedwars.listener;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import de.kybu.achievements.common.IAchievementPlayerProvider;
import de.kybu.achievements.common.model.IAchievementPlayer;
import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.bedwars.game.scoreboard.BedWarsScoreboard;
import de.kybu.bedwars.game.team.BedWarsTeam;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.teams.Team;
import de.kybu.gameframe.teams.TeamService;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Bed;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class BlockBreakHandler implements Listener {

    @EventHandler
    public void handle(final BlockBreakEvent event){

        final Player player = event.getPlayer();
        final BedWarsGame game = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();
        final User user = UserService.getInstance().getUser(player.getUniqueId());

        List<ItemStack> tempDropClone = new ArrayList<>(event.getBlock().getDrops());
        for (ItemStack drop : tempDropClone) {
            if(drop.getType() == Material.BED || drop.getType() == Material.BED_BLOCK)
                event.getBlock().getDrops().remove(drop);
        }

        if(!game.isPlayer(player.getUniqueId())){
            event.setCancelled(true);
            return;
        }

        if(game.getCurrentStatus() != GameStatus.RUNNING){
            event.setCancelled(true);
            return;
        }

        if(game.getPlacedBlocks().contains(event.getBlock())){
            event.setCancelled(false);
            if(event.getBlock().getType() == Material.WEB)
                event.getBlock().getDrops().clear();
            game.removeBlock(event.getBlock());
            return;
        }

        Team playerTeam = TeamService.getInstance().getPlayersTeam(player);
        if(playerTeam == null)
            return;

        if(event.getBlock().getType() == Material.BED_BLOCK){
            event.getBlock().getDrops().clear();

            Bed bed = (Bed) event.getBlock().getState().getData();
            if(bed.isHeadOfBed()){
                bed = (Bed) event.getBlock().getRelative(bed.getFacing().getOppositeFace()).getState().getData();
            }
            Team team = this.findTeamByBed(bed);

            if(team == null){
                event.setCancelled(true);
                return;
            }

            if(team.equals(playerTeam)){
                user.sendTranslatedMessage("cant-break-own-bed", ModuleService.getInstance().getPrefix());
                event.setCancelled(true);
            } else {
                event.setCancelled(false);
                BukkitUtil.broadcastTranslatedMessage("bed-destroyed-by", ModuleService.getInstance().getPrefix(), playerTeam.getTeamColor() + player.getName(), team.getTeamColor() + "Team " + team.getTeamName());
                BedWarsTeam bedWarsTeam = (BedWarsTeam) team.getCustomTeamObject();

                if(System.currentTimeMillis() - game.getStartedAt() <= TimeUnit.MINUTES.toMillis(2)){
                    Futures.addCallback(IAchievementPlayerProvider.getInstance().getAchievementPlayer(player.getUniqueId()), new FutureCallback<IAchievementPlayer>() {
                        @Override
                        public void onSuccess(@Nullable IAchievementPlayer iAchievementPlayer) {
                            if(!iAchievementPlayer.hasAchievement(4))
                                iAchievementPlayer.unlockAchievement(4 /* Rusher Achievement */);
                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    }, GameFrame.getInstance().getExecutorService());
                }

                bedWarsTeam.setHasBed(false);
                team.getAliveTeamMembers().forEach(teamPlayer -> {
                    User tUser = UserService.getInstance().getUser(teamPlayer.getPlayer().getUniqueId());
                    tUser.sendTranslatedMessage("your-bed-destoryed", ModuleService.getInstance().getPrefix(), team.getTeamColor());
                });
                Bukkit.getOnlinePlayers().forEach(players -> players.playSound(players.getLocation(), Sound.WITHER_DEATH, 3, 1));

                BedWarsScoreboard.getInstance().update(game);

                event.setExpToDrop(0);
                event.getBlock().getDrops().clear();
            }

        } else {
            event.setCancelled(true);
        }
    }

    private Team findTeamByBed(final Bed bed){
        for(de.kybu.gameframe.teams.Team team : TeamService.getInstance().getRegisteredTeams().values()){

            BedWarsTeam bedWarsTeam = (BedWarsTeam) team.getCustomTeamObject();

            if(bedWarsTeam.hasBed()){
                if(bedWarsTeam.getBed().equals(bed)){
                    return team;
                }
            }

        }

        return null;
    }

}
