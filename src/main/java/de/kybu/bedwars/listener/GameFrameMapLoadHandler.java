package de.kybu.bedwars.listener;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.bedwars.game.team.BedWarsTeam;
import de.kybu.gameframe.events.GameFrameMapLoadEvent;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.teams.Team;
import de.kybu.gameframe.teams.TeamService;
import de.kybu.gameframe.util.XYZW;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.WorldBorder;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.material.Bed;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Map;

public class GameFrameMapLoadHandler implements Listener {

    @EventHandler
    public void handle(final GameFrameMapLoadEvent event){

        BedWarsGame game = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();
        game.clearSpawnerLocations();

        for(Map.Entry<String, XYZW> entries : event.getMap().getMapLocations().entrySet()){

            if(entries.getKey().startsWith("bed_")){
                int teamId = Integer.parseInt(entries.getKey().split("_")[1]);
                Team team = TeamService.getInstance().getRegisteredTeams().get(teamId);
                if(team.getCustomTeamObject() == null)
                    team.setCustomTeamObject(new BedWarsTeam(null));

                try{
                    Bed bed = (Bed) entries.getValue().toLocation().getBlock().getState().getData();
                    ((BedWarsTeam) team.getCustomTeamObject()).setBed(bed);
                } catch(Exception ex){
                    entries.getValue().toLocation().getBlock().setType(Material.BED);
                    Bed bed = (Bed) entries.getValue().toLocation().getBlock().getState().getData();
                    ((BedWarsTeam) team.getCustomTeamObject()).setBed(bed);
                }



                continue;
            }

            if(entries.getKey().startsWith("bronze")){
                game.addSpawnerLocation(Material.CLAY_BRICK, entries.getValue().toLocation());
                continue;
            }


            if(entries.getKey().startsWith("eisen")){
                game.addSpawnerLocation(Material.IRON_INGOT, entries.getValue().toLocation());
                continue;
            }


            if(entries.getKey().startsWith("gold")){
                game.addSpawnerLocation(Material.GOLD_INGOT, entries.getValue().toLocation());
                continue;
            }


            if(entries.getKey().startsWith("team_")){
                int teamId = Integer.parseInt(entries.getKey().split("_")[1]);
                TeamService.getInstance().getRegisteredTeams().get(teamId).setTeamSpawnLocation(entries.getValue().toLocation());
                continue;
            }

            if(entries.getKey().equals("mid")){
                game.setMidLocation(entries.getValue().toLocation());
                continue;
            }

            if(entries.getKey().startsWith("villager_")){
                int teamId = Integer.parseInt(entries.getKey().split("_")[1]);
                Team team = TeamService.getInstance().getRegisteredTeams().get(teamId);
                if(team.getCustomTeamObject() == null)
                    team.setCustomTeamObject(new BedWarsTeam(null));

                Location villagerLocation = entries.getValue().toLocation();
                BedWarsTeam bedWarsTeam = (BedWarsTeam) team.getCustomTeamObject();
                Villager villager = villagerLocation.getWorld().spawn(villagerLocation, Villager.class);
                villager.setRemoveWhenFarAway(false);
                villager.setCanPickupItems(false);
                villager.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 100, false, false));


                bedWarsTeam.setShopVillager(villager);
            }
        }

        WorldBorder worldBorder = Bukkit.getWorld(event.getMap().getMapName()).getWorldBorder();
        worldBorder.setCenter(game.getMidLocation());
        worldBorder.setDamageBuffer(2);
        worldBorder.setSize(event.getMap().getWorldBorderMaximumSize());

    }

}
