package de.kybu.bedwars.listener;

import de.kybu.bedwars.BedWars;
import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class BlockPlaceHandler implements Listener {

    private final BedWarsGame game;

    public BlockPlaceHandler(){
        this.game = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();
    }

    @EventHandler
    public void handle(final BlockPlaceEvent event){

        if(game.getCurrentStatus() != GameStatus.RUNNING){
            event.setCancelled(true);
            return;
        }

        game.addBlock(event.getBlockPlaced());

        if(event.getBlockPlaced().getType() == Material.CHEST){
            if(event.getItemInHand().hasItemMeta() && event.getItemInHand().getItemMeta().getDisplayName().equals("§fFake Truhe")){
                event.getBlockPlaced().setMetadata("fakechest", new FixedMetadataValue(BedWars.getInstance(), "lol"));
            }
        }
    }

}
