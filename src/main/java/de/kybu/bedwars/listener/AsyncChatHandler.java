package de.kybu.bedwars.listener;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.teams.Team;
import de.kybu.gameframe.teams.TeamService;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 06.02.2021
 */
public class AsyncChatHandler implements Listener {

    @EventHandler
    public void handle(final AsyncPlayerChatEvent event){
        final Player player = event.getPlayer();
        final BedWarsGame bedWarsGame = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();

        if(event.getMessage().contains("%"))
            event.setMessage(event.getMessage().replace("%", ""));

        switch (bedWarsGame.getCurrentStatus()){
            case LOBBY:
            case ENDING:
                event.setFormat("§c" + player.getName() + "§7: §f" + event.getMessage());
                break;
            case RUNNING:

                if(bedWarsGame.isPlayer(player.getUniqueId())){
                    Team playerTeam = TeamService.getInstance().getPlayersTeam(player);
                    if(bedWarsGame.getGameSize().getPlayersPerTeam() == 1){
                        event.setMessage("@all " + event.getMessage());
                    }

                    if(event.getMessage().startsWith("@all ")){
                        event.setCancelled(false);
                        event.setFormat("§8[§7@all§8] " + playerTeam.getTeamColor() + player.getName() + "§7: §f" + event.getMessage().replace("@all ", ""));
                    } else {
                        event.setCancelled(true);
                        playerTeam.getTeamMembers().forEach((uuid, teamPlayer) -> {
                            teamPlayer.getPlayer().sendMessage(playerTeam.getTeamColor() + player.getName() + "§7: §f" + event.getMessage());
                        });
                    }
                } else {
                    event.setCancelled(true);
                    UserService.getInstance().getOnlineUsers()
                            .stream()
                            .filter(user -> !bedWarsGame.isPlayer(user.getUuid()))
                            .forEach(user -> {
                                user.sendMessage("§7" + player.getName() + ": §f" + event.getMessage());
                            });
                }

                break;
        }
    }

}
