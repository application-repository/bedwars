package de.kybu.bedwars.listener;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class FoodLevelChangeHandler implements Listener {

    @EventHandler
    public void handle(final FoodLevelChangeEvent event){
        if(ModuleService.getInstance().getActivatedModule().getGame().getCurrentStatus() != GameStatus.RUNNING)
            event.setCancelled(true);
        else {
            BedWarsGame bedWarsGame = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();
            event.setCancelled(!bedWarsGame.isPlayer(event.getEntity().getUniqueId()));
        }

    }

}
