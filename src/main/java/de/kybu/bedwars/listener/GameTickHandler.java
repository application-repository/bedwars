package de.kybu.bedwars.listener;

import de.kybu.bedwars.events.GameTickEvent;
import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.teams.Team;
import de.kybu.gameframe.teams.TeamService;
import de.kybu.gameframe.util.message.ActionBarMessage;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.text.SimpleDateFormat;

public class GameTickHandler implements Listener {

    private final SimpleDateFormat simpleDateFormat;
    private BedWarsGame game;

    public GameTickHandler(){
        this.simpleDateFormat = new SimpleDateFormat("'§8[§7'mm:ss'§8]'");
        this.game = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();
    }

    @EventHandler
    public void handle(final GameTickEvent event){
        String time = this.simpleDateFormat.format(System.currentTimeMillis() - event.getStartedAt());

        for(User user : UserService.getInstance().getOnlineUsers()){
            if(game.isPlayer(user.getUuid())){
                Team team = TeamService.getInstance().getTeamPlayers().get(user.getUuid());
                user.sendMessage(new ActionBarMessage(team.getTeamColor() + "Team " + team.getTeamName() + " " + time));
            } else {
                String spectatorTranslation = user.getPlayerLanguage().getTranslation("spectator");
                user.sendMessage(new ActionBarMessage("§7" + spectatorTranslation + " " + time));
            }
        }
    }

}
