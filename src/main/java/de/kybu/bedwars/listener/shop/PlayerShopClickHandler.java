package de.kybu.bedwars.listener.shop;

import de.kybu.bedwars.BedWars;
import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.bedwars.shop.BedWarsShopMenuHelper;
import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

public class PlayerShopClickHandler implements Listener {

    @EventHandler
    public void handle(final PlayerInteractAtEntityEvent event){

        if(event.getRightClicked().getType() != EntityType.VILLAGER)
            return;

        event.setCancelled(true);

        BedWarsGame game = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();

        if(game.getCurrentStatus() != GameStatus.RUNNING)
            return;

        if(!game.isPlayer(event.getPlayer().getUniqueId()))
            return;

        Bukkit.getScheduler().runTaskLater(BedWars.getInstance(), () -> {
            BedWarsShopMenuHelper.getInstance().openShop(event.getPlayer());
        }, 2);

    }

}
