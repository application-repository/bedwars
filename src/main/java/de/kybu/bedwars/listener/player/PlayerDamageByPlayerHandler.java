package de.kybu.bedwars.listener.player;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.CombatLog;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class PlayerDamageByPlayerHandler implements Listener {

    @EventHandler
    public void handle(final EntityDamageByEntityEvent event){

        if(event.getEntity().getType() != EntityType.PLAYER)
            return;

        Player player = (Player) event.getEntity();
        BedWarsGame game = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();

        if(game.getCurrentStatus() == GameStatus.RUNNING){
            if(!game.isPlayer(player.getUniqueId())){
                event.setDamage(0);
                event.setCancelled(true);
                return;
            }

            Player damager = this.getDamager(event.getDamager());
            if(damager == null)
                return;

            if(player.getUniqueId().equals(damager.getUniqueId())){
                event.setCancelled(true);
                return;
            }


            if(!game.isPlayer(damager.getUniqueId())){
                event.setDamage(0);
                event.setCancelled(true);
                return;
            }

            if(CombatLog.getInstance().isInCombat(player))
                CombatLog.getInstance().removePlayer(player);

            CombatLog.getInstance().addFightingPlayers(player, damager);
        }
    }

    private Player getDamager(Entity entity){

        if(entity.getType() == EntityType.ARROW){
            Arrow arrow = (Arrow) entity;
            if(!(arrow.getShooter() instanceof Player))
                return null;

            return (Player) arrow.getShooter();
        } else if(entity.getType() == EntityType.PLAYER){
            return (Player) entity;
        }

        return null;

    }

}
