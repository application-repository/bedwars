package de.kybu.bedwars.listener.player;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.bedwars.game.scoreboard.BedWarsLobbyScoreboard;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.item.items.AchievementsItem;
import de.kybu.gameframe.item.items.TeamSelectionItem;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinHandler implements Listener {

    @EventHandler
    public void handle(final PlayerJoinEvent event){
        final BedWarsGame game = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();
        final Player player = event.getPlayer();
        final User user = UserService.getInstance().getUser(player.getUniqueId());

        player.setMaxHealth(20);
        player.setHealth(20);

        switch (game.getCurrentStatus()){
            case ENDING:
                player.kickPlayer(user.getPlayerLanguage().getTranslation("round-ending"));
                break;
            case RUNNING:
                user.sendTranslatedMessage("game-already-started", ModuleService.getInstance().getPrefix());
                game.setSpectator(player);
                break;
            case LOBBY:
                if(game.getBedWarsPlayerMap().size() >= game.getGameSize().getAllowedPlayers()){
                    player.kickPlayer(user.getPlayerLanguage().getTranslation("round-full"));
                    return;
                }
                for (User onlineUser : UserService.getInstance().getOnlineUsers()) {
                    onlineUser.sendMessage("§8» §c" + player.getName() + onlineUser.getPlayerLanguage().getTranslation("player-joined"));
                }
                game.registerPlayer(player);
                game.startPrepare();
                BedWarsLobbyScoreboard.getInstance().update(game);
                BedWarsLobbyScoreboard.getInstance().setScoreboard(player);
                BukkitUtil.showAllHiddenPlayers(player);
                BukkitUtil.clearInventory(player);
                BukkitUtil.resetPlayer(player, true, true);
                player.setGameMode(GameMode.SURVIVAL);
                player.teleport(GameFrame.getInstance().getGameFrameConfiguration().getSpawnLocation().toLocation());
                player.getInventory().setItem(0, TeamSelectionItem.getInstance().build());
                player.getInventory().setItem(8, AchievementsItem.getInstance().build());
                break;
        }
    }
}
