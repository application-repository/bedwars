package de.kybu.bedwars.listener.player;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.bedwars.game.player.BedWarsPlayer;
import de.kybu.bedwars.game.scoreboard.BedWarsLobbyScoreboard;
import de.kybu.bedwars.game.scoreboard.BedWarsScoreboard;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.teams.Team;
import de.kybu.gameframe.teams.TeamService;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.gameframe.util.CombatLog;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitHandler implements Listener {

    @EventHandler
    public void handle(final PlayerQuitEvent event){

        event.setQuitMessage(null);

        final Player player = event.getPlayer();
        final BedWarsGame game = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();

        switch (game.getCurrentStatus()){
            case LOBBY:
                for (User user : UserService.getInstance().getOnlineUsers()) {
                    user.sendMessage("§7« §c" + player.getName() + " §7" + user.getPlayerLanguage().getTranslation("player-left"));
                }
                game.dropPlayer(player.getUniqueId());
                BedWarsLobbyScoreboard.getInstance().update(game);
                game.startPrepare();
                break;
            case RUNNING:

                if(game.isPlayer(player.getUniqueId())){
                    Team playerTeam = TeamService.getInstance().getPlayersTeam(player);
                    BedWarsPlayer bedWarsPlayer = game.getBedWarsPlayerMap().get(player.getUniqueId());
                    bedWarsPlayer.setTotalDeaths(bedWarsPlayer.getTotalDeaths() + 1);
                    bedWarsPlayer.setAlive(false);
                    playerTeam.killTeamMember(player.getUniqueId());

                    BukkitUtil.broadcastTranslatedMessage("ig-player-left", ModuleService.getInstance().getPrefix(), playerTeam.getTeamColor() + player.getName());

                    if(playerTeam.getAliveTeamMembers().isEmpty()){
                        BukkitUtil.broadcastTranslatedMessage("bw-team-death", ModuleService.getInstance().getPrefix(), playerTeam.getTeamColor() + "Team " + playerTeam.getTeamName());
                    }

                    Player killer = CombatLog.getInstance().getMap().get(player);
                    if(killer != null){
                        UserService.getInstance().getUser(killer.getUniqueId()).sendTranslatedMessage("bw-you-killed-player", ModuleService.getInstance().getPrefix(), playerTeam.getTeamColor() + player.getName());
                        killer.playSound(killer.getLocation(), Sound.LEVEL_UP, 3, 1);
                    }

                    BedWarsScoreboard.getInstance().update(game);
                    game.isWinner();
                }

                game.dropPlayer(player.getUniqueId());

                break;
            case ENDING:
                game.dropPlayer(player.getUniqueId());
                break;
        }

    }

}
