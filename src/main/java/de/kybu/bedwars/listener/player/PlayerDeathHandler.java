package de.kybu.bedwars.listener.player;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import de.kybu.achievements.common.IAchievementPlayerProvider;
import de.kybu.achievements.common.model.IAchievementPlayer;
import de.kybu.bedwars.BedWars;
import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.bedwars.game.player.BedWarsPlayer;
import de.kybu.bedwars.game.scoreboard.BedWarsScoreboard;
import de.kybu.bedwars.game.team.BedWarsTeam;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.teams.Team;
import de.kybu.gameframe.teams.TeamService;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.gameframe.util.CombatLog;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import javax.annotation.Nullable;

public class PlayerDeathHandler implements Listener {

    @EventHandler
    public void handle(final PlayerDeathEvent event){
        final Player player = event.getEntity().getPlayer();

        event.setKeepInventory(false);
        event.setDeathMessage(null);
        event.getDrops().clear();

        BedWarsGame game = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();

        if(!game.isPlayer(player.getUniqueId()))
            return;


        if(game.getCurrentStatus() == GameStatus.RUNNING){
            Team playerTeam = TeamService.getInstance().getPlayersTeam(player);
            BedWarsTeam bedWarsTeam = (BedWarsTeam) playerTeam.getCustomTeamObject();
            Player killer = CombatLog.getInstance().getMap().get(player);

            BedWarsPlayer bedWarsPlayer = game.getBedWarsPlayerMap().get(player.getUniqueId());
            bedWarsPlayer.setTotalDeaths(bedWarsPlayer.getTotalDeaths() + 1);

            this.sendDeathMessage(playerTeam, player, killer, !bedWarsTeam.hasBed(), game);

            if(!bedWarsTeam.hasBed()){
                playerTeam.killTeamMember(player.getUniqueId());
                BedWarsScoreboard.getInstance().update(game);
                bedWarsPlayer.setAlive(false);
                if(playerTeam.getAliveTeamMembers().size() == 0){
                    BukkitUtil.broadcastTranslatedMessage("bw-team-death", ModuleService.getInstance().getPrefix(), playerTeam.getTeamColor() + "Team " + playerTeam.getTeamName());
                }
            }
        }

        CombatLog.getInstance().removePlayer(player);
        Bukkit.getScheduler().runTaskLater(BedWars.getInstance(), () -> player.spigot().respawn(), 20);

    }

    public void sendDeathMessage(final Team playerTeam, final Player player, final Player killer, boolean finalKill, BedWarsGame game){
        User user = UserService.getInstance().getUser(player.getUniqueId());
        if(killer == null){
            BukkitUtil.broadcastTranslatedMessage("bw-bc-player-died-no-killer", ModuleService.getInstance().getPrefix(), playerTeam.getTeamColor() + player.getName());
            user.sendTranslatedMessage("you-died-lol", ModuleService.getInstance().getPrefix());

            Futures.addCallback(IAchievementPlayerProvider.getInstance().getAchievementPlayer(player.getUniqueId()), new FutureCallback<IAchievementPlayer>() {
                @Override
                public void onSuccess(@Nullable IAchievementPlayer iAchievementPlayer) {
                    if(!iAchievementPlayer.hasAchievement(1))
                        iAchievementPlayer.unlockAchievement(1 /* Selbstmord Achievement */);
                }

                @Override
                public void onFailure(Throwable throwable) {
                    throwable.printStackTrace();
                }
            }, GameFrame.getInstance().getExecutorService());
        } else {
            if(game.isFirstKill()){
                game.setFirstKill(false);
                Futures.addCallback(IAchievementPlayerProvider.getInstance().getAchievementPlayer(killer.getUniqueId()), new FutureCallback<IAchievementPlayer>() {
                    @Override
                    public void onSuccess(@Nullable IAchievementPlayer iAchievementPlayer) {
                        if(!iAchievementPlayer.hasAchievement(2))
                            iAchievementPlayer.unlockAchievement(2 /* First Kill Achievement */);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }, GameFrame.getInstance().getExecutorService());
            }
            User killerUser = UserService.getInstance().getUser(killer.getUniqueId());
            Team killerTeam = TeamService.getInstance().getPlayersTeam(killer);
            BukkitUtil.broadcastTranslatedMessage("bw-bc-player-killed-by-player", ModuleService.getInstance().getPrefix(), playerTeam.getTeamColor() + player.getName(), killerTeam.getTeamColor() + killer.getName());
            user.sendTranslatedMessage("bw-killed-by-player", ModuleService.getInstance().getPrefix(), killerTeam.getTeamColor() + killer.getName());
            killerUser.sendTranslatedMessage("bw-you-killed-player", ModuleService.getInstance().getPrefix(), playerTeam.getTeamColor() + player.getName());
            if(finalKill){
                killer.playSound(killer.getLocation(), Sound.LEVEL_UP, 3, 1);
                BedWarsPlayer killerBWP = game.getBedWarsPlayerMap().get(killer.getUniqueId());
                killerBWP.setFinalKills(killerBWP.getFinalKills() + 1);
            }
        }
    }

}
