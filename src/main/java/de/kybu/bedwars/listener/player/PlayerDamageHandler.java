package de.kybu.bedwars.listener.player;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class PlayerDamageHandler implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(final EntityDamageEvent event){

        BedWarsGame game = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();

        if(game.getCurrentStatus() != GameStatus.RUNNING){
            event.setCancelled(true);
            return;
        }

        if(!(event.getEntity() instanceof Player))
            return;

        Player player = (Player) event.getEntity();

        if(!game.isPlayer(player.getUniqueId())){
            event.setCancelled(true);
        }

    }

}
