package de.kybu.bedwars.listener.player;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveHandler implements Listener {

    private final BedWarsGame game;

    public PlayerMoveHandler(){
        this.game = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();
    }

    @EventHandler
    public void handle(final PlayerMoveEvent event){

        if(event.getPlayer().getLocation().getY() <= 0){

            if(!game.isPlayer(event.getPlayer().getUniqueId()))
                event.getPlayer().teleport(game.getMidLocation());

        }

    }

}
