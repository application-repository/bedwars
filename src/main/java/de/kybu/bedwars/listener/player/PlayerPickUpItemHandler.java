package de.kybu.bedwars.listener.player;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PlayerPickUpItemHandler implements Listener {

    @EventHandler
    public void handle(final PlayerPickupItemEvent event){

        if(event.getItem().getItemStack().getType() == Material.ARROW){
            event.setCancelled(true);
            return;
        }

        BedWarsGame game = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();
        if(game.getCurrentStatus() != GameStatus.RUNNING)
            return;

        if(!game.isPlayer(event.getPlayer().getUniqueId())){
            event.setCancelled(true);
            return;
        }

        switch (event.getItem().getItemStack().getType()){
            case CLAY_BRICK:
                ItemStack bronzeStack = event.getItem().getItemStack();
                ItemMeta bronzeMeta = bronzeStack.getItemMeta();
                bronzeMeta.setDisplayName("§cBronze");
                bronzeStack.setItemMeta(bronzeMeta);
                break;
            case IRON_INGOT:
                ItemStack itemStack = event.getItem().getItemStack();
                ItemMeta itemMeta = itemStack.getItemMeta();
                itemMeta.setDisplayName("§fEisen");
                itemStack.setItemMeta(itemMeta);
                break;
            case GOLD_INGOT:
                ItemStack goldItem = event.getItem().getItemStack();
                ItemMeta goldMeta = goldItem.getItemMeta();
                goldMeta.setDisplayName("§6Gold");
                goldItem.setItemMeta(goldMeta);
                break;
        }

    }

}
