package de.kybu.bedwars.listener.player;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.teams.Team;
import de.kybu.gameframe.teams.TeamPlayer;
import de.kybu.gameframe.teams.TeamService;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.util.Vector;

public class PlayerRespawnHandler implements Listener {

    @EventHandler
    public void handle(final PlayerRespawnEvent event){

        BedWarsGame game = (BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame();
        Team team = TeamService.getInstance().getPlayersTeam(event.getPlayer());
        event.getPlayer().setVelocity(new Vector(0, 0, 0));

        if(team == null){
            event.setRespawnLocation(getSpawnLocation(event.getPlayer(), game, null, team));
        } else {
            TeamPlayer teamPlayer = team.getTeamMembers().get(event.getPlayer().getUniqueId());
            event.setRespawnLocation(getSpawnLocation(event.getPlayer(), game, teamPlayer, team));

            if(!teamPlayer.isAlive()){
                game.setSpectator(event.getPlayer());
                event.getPlayer().updateInventory();
                if(game.isWinner() != null)
                    event.setRespawnLocation(GameFrame.getInstance().getGameFrameConfiguration().getSpawnLocation().toLocation());
            }
        }

    }

    public Location getSpawnLocation(final Player player, BedWarsGame game, TeamPlayer teamPlayer, Team team){
        if(game.getCurrentStatus() == GameStatus.RUNNING){

            if(game.isPlayer(player.getUniqueId())){


                if(teamPlayer.isAlive()){
                    return team.getTeamSpawnLocation();
                } else {
                    return game.getMidLocation();
                }

            } else {
                return game.getMidLocation();
            }

        } else {
            return GameFrame.getInstance().getGameFrameConfiguration().getSpawnLocation().toLocation();
        }
    }

}
