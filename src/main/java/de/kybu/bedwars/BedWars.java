package de.kybu.bedwars;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.gameframe.game.spawner.Spawner;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.modules.misc.GameSize;
import de.kybu.gameframe.modules.misc.Module;
import org.bukkit.plugin.java.JavaPlugin;

public class BedWars extends JavaPlugin {

    private static BedWars instance;

    public static BedWars getInstance() {
        return instance;
    }

    @Override
    public void onLoad() {
        super.onLoad();
    }

    @Override
    public void onEnable() {
        instance = this;

        Module thisModule = ModuleService.getInstance().getActivatedModule();
        GameSize gameSize = thisModule.getModuleConfiguration().getAllowedGameSize().get(0);
        thisModule.setGame(new BedWarsGame());
        ModuleService.getInstance().registerAllListenerInPackage("de.kybu.bedwars.listener", this, this.getClassLoader());

        thisModule.getGame().setGameSize(gameSize);
        thisModule.getGame().resetGame();
    }

    @Override
    public void onDisable() {
        super.onDisable();
        Spawner.deleteAll();
        ((BedWarsGame) ModuleService.getInstance().getActivatedModule().getGame()).stopGameTimer();
    }
}
