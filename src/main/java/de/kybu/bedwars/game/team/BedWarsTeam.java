package de.kybu.bedwars.game.team;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Villager;
import org.bukkit.material.Bed;

@Getter
@Setter
public class BedWarsTeam {

    private boolean hasBed;
    private Bed bed;
    private Villager shopVillager;

    public BedWarsTeam(final Bed bed){
        this.bed = bed;
        this.hasBed = true;
    }

    public boolean hasBed() {
        return hasBed;
    }
}
