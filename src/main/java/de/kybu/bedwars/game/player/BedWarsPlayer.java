package de.kybu.bedwars.game.player;

import de.kybu.gameframe.modules.ModuleService;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;

@Getter
@Setter
public class BedWarsPlayer {

    private Player mainObject;
    private int finalKills;
    private int brokenBeds;
    private int totalDeaths;

    private boolean alive;

    public BedWarsPlayer(final Player player){
        this.mainObject = player;
        this.finalKills = 0;
        this.brokenBeds = 0;
        this.totalDeaths = 0;

        this.alive = true;
    }

    public void printRoundStats(){
        Player player = this.mainObject;
        player.sendMessage(ModuleService.getInstance().getPrefix() + "§8§m-----§8=§e Stats §8=§m-----");
        player.sendMessage(ModuleService.getInstance().getPrefix() + "§7Final Kills§8: §e" + this.finalKills);
        player.sendMessage(ModuleService.getInstance().getPrefix() + "§7Tode§8: §e" + this.totalDeaths);
        player.sendMessage(ModuleService.getInstance().getPrefix() + "§7Zerstöre Betten§8: §e" + this.brokenBeds);
        player.sendMessage(ModuleService.getInstance().getPrefix() + "§7Überlebt§8: " + (this.alive ? "§aJa" : "§cNein"));
        player.sendMessage(ModuleService.getInstance().getPrefix() + "§8§m-----------------------");

    }
}
