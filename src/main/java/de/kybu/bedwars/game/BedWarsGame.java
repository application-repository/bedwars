package de.kybu.bedwars.game;

import de.kybu.bedwars.BedWars;
import de.kybu.bedwars.countdowns.EndCountdown;
import de.kybu.bedwars.countdowns.LobbyCountdown;
import de.kybu.bedwars.events.GameTickEvent;
import de.kybu.bedwars.game.player.BedWarsPlayer;
import de.kybu.bedwars.game.scoreboard.BedWarsLobbyScoreboard;
import de.kybu.bedwars.game.scoreboard.BedWarsScoreboard;
import de.kybu.bedwars.shop.BedWarsShopMenuHelper;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.game.Game;
import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.game.countdown.Countdown;
import de.kybu.gameframe.game.inventory.inventories.SpectatorInventory;
import de.kybu.gameframe.game.inventory.inventories.TeamSelectionInventory;
import de.kybu.gameframe.game.spawner.Spawner;
import de.kybu.gameframe.item.items.AchievementsItem;
import de.kybu.gameframe.item.items.SpectatorTeleporterItem;
import de.kybu.gameframe.item.items.TeamSelectionItem;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.teams.Team;
import de.kybu.gameframe.teams.TeamService;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.github.paperspigot.Title;

import java.util.*;

@Getter
@Setter
public class BedWarsGame extends Game {

    private final Map<UUID, BedWarsPlayer> bedWarsPlayerMap;
    private final Map<Material, List<Location>> spawnerLocations;
    private Location midLocation;
    private final List<Block> placedBlocks;
    private long startedAt;
    private boolean firstKill;

    private Timer gameTimer;

    public BedWarsGame() {
        super(true);
        this.bedWarsPlayerMap = new HashMap<>();
        this.spawnerLocations = new HashMap<>();
        this.placedBlocks = new ArrayList<>();
        this.firstKill = true;
    }

    @Override
    public void startPrepare() {
        if(!isReady()){
            if(this.getCurrentCountdown() != null){
                this.getCurrentCountdown().stop();
                this.setCurrentCountdown(null);
                BukkitUtil.broadcastTranslatedMessage("countdown-aborted", ModuleService.getInstance().getPrefix());
            }
            return;
        }

        if(this.getCurrentCountdown() == null){
            BukkitUtil.broadcastTranslatedMessage("countdown-started", ModuleService.getInstance().getPrefix());
            Countdown countdown = new LobbyCountdown();
            countdown.start();
            this.setCurrentCountdown(countdown);
        }

    }

    @Override
    public void startGame() {
        this.startGameTimer();
        this.setCurrentStatus(GameStatus.RUNNING);
        TeamService.getInstance().assignPlayersRandom();
        BedWarsScoreboard.getInstance().create(this);
        BedWarsShopMenuHelper.getInstance();

        for(Player player : Bukkit.getOnlinePlayers()){
            BukkitUtil.clearInventory(player);
            player.setGameMode(GameMode.SURVIVAL);
            player.teleport(TeamService.getInstance().getPlayersTeam(player).getTeamSpawnLocation());
            BedWarsScoreboard.getInstance().setToPlayer(player);
        }

        for(Map.Entry<Material, List<Location>> spawners : this.spawnerLocations.entrySet()){
            for (Location location : spawners.getValue()) {
                Spawner spawner = new Spawner(spawners.getKey(), location);
                spawner.createSpawner();
            }
        }
        Spawner.startSpawner(Material.CLAY_BRICK, 1);
        Spawner.startSpawner(Material.IRON_INGOT, 10);
        Spawner.startSpawner(Material.GOLD_INGOT, 30);
        this.updateSpectatorInventory();
    }

    @Override
    public void stopGame() {
        Spawner.deleteAll();
        this.setCurrentStatus(GameStatus.ENDING);

        Countdown countdown = new EndCountdown();
        countdown.start();
        this.setCurrentCountdown(countdown);

        this.stopGameTimer();
    }

    @Override
    public void resetGame() {
        this.bedWarsPlayerMap.clear();
        TeamService.getInstance().reset();
        Spawner.deleteAll();
        firstKill = true;

        AchievementsItem.getInstance().getAchievementsInventoryMap().clear();

        this.setCurrentCountdown(null);
        this.setCurrentStatus(GameStatus.LOBBY);

        Location spawnLocation = GameFrame.getInstance().getGameFrameConfiguration().getSpawnLocation().toLocation();

        Bukkit.getOnlinePlayers().forEach(player -> {
            if(this.bedWarsPlayerMap.size() >= this.getGameSize().getAllowedPlayers()){
                player.kickPlayer("§7Das Game ist voll");
                return;
            }

            BukkitUtil.resetPlayer(player, true, true);
            BukkitUtil.clearInventory(player);
            BukkitUtil.showAllHiddenPlayers(player);
            BedWarsLobbyScoreboard.getInstance().setScoreboard(player);

            for (User onlineUser : UserService.getInstance().getOnlineUsers()) {
                onlineUser.sendMessage("§8» §c" + player.getName() + onlineUser.getPlayerLanguage().getTranslation("player-joined"));
            }
            player.getInventory().setItem(0, TeamSelectionItem.getInstance().build());
            player.getInventory().setItem(8, AchievementsItem.getInstance().build());
            this.registerPlayer(player);
            this.startPrepare();

            player.teleport(spawnLocation);
        });

        this.registerTeams();
        TeamSelectionInventory.getInstance().fillUpInventory();
        this.loadMap();
        BedWarsLobbyScoreboard.getInstance().update(this);
    }

    public void registerPlayer(final Player player){
        this.bedWarsPlayerMap.put(player.getUniqueId(), new BedWarsPlayer(player));
    }

    public void updateSpectatorInventory(){
        List<Player> list = new ArrayList<>();
        this.bedWarsPlayerMap.forEach((uuid, bedWarsPlayer) -> list.add(bedWarsPlayer.getMainObject()));
        SpectatorInventory.getInstance().updateInventory(list);
    }

    public Map<UUID, BedWarsPlayer> getBedWarsPlayerMap() {
        return bedWarsPlayerMap;
    }

    public void dropPlayer(final UUID uuid){
        this.bedWarsPlayerMap.remove(uuid);
        this.updateSpectatorInventory();
    }

    public boolean isPlayer(final UUID uuid){
        return this.bedWarsPlayerMap.containsKey(uuid) && this.bedWarsPlayerMap.get(uuid).isAlive();
    }

    public Team isWinner(){
        final List<Team> remainingTeams = new ArrayList<>();
        for(Team team : TeamService.getInstance().getRegisteredTeams().values()){
            if(team.getAliveTeamMembers().isEmpty())
                continue;

            remainingTeams.add(team);
        }

        if(remainingTeams.isEmpty())
            return null;

        if(remainingTeams.size() > 1)
            return null;

        Team winnerTeam = remainingTeams.stream().findFirst().get();
        this.stopGame();

        Title winnerTitle = Title.builder()
                .title(winnerTeam.getTeamColor() + "Team " + winnerTeam.getTeamName())
                .subtitle("§7hat das Spiel gewonnen!")
                .fadeIn(10)
                .stay(20)
                .fadeOut(10)
                .build();
        Location spawnLocation = GameFrame.getInstance().getGameFrameConfiguration().getSpawnLocation().toLocation();

        Bukkit.getScheduler().runTaskLater(BedWars.getInstance(), () -> {
            Bukkit.getOnlinePlayers().forEach(player -> {
                player.sendTitle(winnerTitle);
                BukkitUtil.resetPlayer(player, true, true);
                BukkitUtil.showAllHiddenPlayers(player);
                player.teleport(spawnLocation);
                player.playSound(player.getLocation(), Sound.LEVEL_UP, 3, 1);

                if(this.bedWarsPlayerMap.containsKey(player.getUniqueId()))
                    this.bedWarsPlayerMap.get(player.getUniqueId()).printRoundStats();

                BedWarsLobbyScoreboard.getInstance().setScoreboard(player);
            });
            BedWarsLobbyScoreboard.getInstance().update(this);
        }, 10);

        return winnerTeam;
    }

    public void addSpawnerLocation(final Material material, final Location location){
        if(!this.spawnerLocations.containsKey(material))
            this.spawnerLocations.put(material, new ArrayList<>());

        this.spawnerLocations.get(material).add(location);
    }

    public void clearSpawnerLocations(){
        this.spawnerLocations.clear();
    }

    public boolean isReady(){
        //return true;
        return this.bedWarsPlayerMap.size() >= 2;
    }

    public void addBlock(final Block block){
        this.placedBlocks.add(block);
    }

    public void setSpectator(final Player player){
        BukkitUtil.clearInventory(player);
        BukkitUtil.resetPlayer(player, true, true);
        player.setAllowFlight(true);
        player.setFlying(true);
        player.teleport(this.getMidLocation());

        this.updateSpectatorInventory();

        BedWarsScoreboard.getInstance().setSpectator(player);
        player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1, false, false));
        BukkitUtil.hidePlayers(player, players -> isPlayer(players.getUniqueId()));

        player.getInventory().setItem(0, SpectatorTeleporterItem.getInstance().build());
    }

    public void removeBlock(final Block block){
        this.placedBlocks.remove(block);
    }

    public void startGameTimer(){
        if(this.gameTimer != null)
            stopGameTimer();

        this.gameTimer = new Timer();
        this.startedAt = System.currentTimeMillis();
        this.gameTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Bukkit.getPluginManager().callEvent(new GameTickEvent(startedAt));
            }
        }, 1000, 1000);
    }

    public boolean isFirstKill() {
        return firstKill;
    }

    public void setFirstKill(boolean firstKill) {
        this.firstKill = firstKill;
    }

    public void stopGameTimer(){
        if(this.gameTimer == null)
            return;

        this.gameTimer.cancel();
        this.gameTimer.purge();
        this.gameTimer = null;
    }
}
