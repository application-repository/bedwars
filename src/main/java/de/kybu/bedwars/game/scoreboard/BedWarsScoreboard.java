package de.kybu.bedwars.game.scoreboard;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.bedwars.game.team.BedWarsTeam;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.scoreboard.GameBoard;
import de.kybu.gameframe.teams.Team;
import de.kybu.gameframe.teams.TeamService;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Scoreboard;

public class BedWarsScoreboard {

    private static BedWarsScoreboard instance;

    @Getter @Setter
    private Scoreboard gameScoreboard;

    public static BedWarsScoreboard getInstance() {
        return (instance != null ? instance : (instance = new BedWarsScoreboard()));
    }

    public BedWarsScoreboard(){
        this.gameScoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
    }

    public void create(BedWarsGame game){
        this.gameScoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        GameBoard gameBoard = new GameBoard(this.gameScoreboard);
        gameBoard.display();

        int maxTeamSize = game.getGameSize().getPlayersPerTeam();
        int teamIndex = 7;

        for(Team team : TeamService.getInstance().getRegisteredTeams().values()){
            BedWarsTeam bedWarsTeam = (BedWarsTeam) team.getCustomTeamObject();
            gameBoard.set(teamIndex, (bedWarsTeam.hasBed() ? "§a✔" : "§c〤") + " " + team.getTeamColor() + team.getTeamName(), team.getAliveTeamMembers().size());
            teamIndex++;
        }

        gameBoard.set(maxTeamSize + 1, "§f");
        gameBoard.set(maxTeamSize + 2, "§7Map§8: §e" + ModuleService.getInstance().getActivatedModule().getGame().getMap().getMapName());
        gameBoard.set(maxTeamSize + 3, "§c");
    }

    public synchronized void update(BedWarsGame game){
        GameBoard gameBoard = new GameBoard(this.gameScoreboard);
        if(gameBoard.scoreboard().getObjective(DisplaySlot.SIDEBAR) != null)
            gameBoard.scoreboard().getObjective(DisplaySlot.SIDEBAR).unregister();

        gameBoard.display();

        int maxTeamSize = game.getGameSize().getPlayersPerTeam();
        int teamIndex = 7;

        for(Team team : TeamService.getInstance().getRegisteredTeams().values()){
            BedWarsTeam bedWarsTeam = (BedWarsTeam) team.getCustomTeamObject();
            gameBoard.set(teamIndex, (bedWarsTeam.hasBed() ? "§a✔" : "§c〤") + " " + team.getTeamColor() + team.getTeamName(), team.getAliveTeamMembers().size());
            teamIndex++;
        }

        gameBoard.set(maxTeamSize + 1, "§f");
        gameBoard.set(maxTeamSize + 2, "§7Map§8: §e" + ModuleService.getInstance().getActivatedModule().getGame().getMap().getMapName());
        gameBoard.set(maxTeamSize + 3, "§c");
    }

    public synchronized void setToPlayer(final Player player){

        if(this.gameScoreboard.getEntryTeam(player.getName()) != null)
            this.gameScoreboard.getEntryTeam(player.getName()).removeEntry(player.getName());

        Team playerTeam = TeamService.getInstance().getPlayersTeam(player);
        if(playerTeam == null){

            if(this.gameScoreboard.getTeam("0000spectator") == null){
                org.bukkit.scoreboard.Team team = this.gameScoreboard.registerNewTeam("0000spectator");
                team.setPrefix("§7");
                team.setCanSeeFriendlyInvisibles(true);
            }

            this.gameScoreboard.getTeam("0000spectator").addEntry(player.getName());

        } else {

            if(this.gameScoreboard.getTeam(playerTeam.getTeamName()) == null){
                org.bukkit.scoreboard.Team team = this.gameScoreboard.registerNewTeam(playerTeam.getTeamName());
                team.setAllowFriendlyFire(false);
                team.setPrefix(playerTeam.getTeamColor() + playerTeam.getTeamName() + " §7| " + playerTeam.getTeamColor());
            }

            this.gameScoreboard.getTeam(playerTeam.getTeamName()).addEntry(player.getName());
        }

        player.setScoreboard(this.gameScoreboard);
    }

    public void setSpectator(final Player player){
        if(this.gameScoreboard.getEntryTeam(player.getName()) != null){
            this.gameScoreboard.getEntryTeam(player.getName()).removeEntry(player.getName());
        }


        if(this.gameScoreboard.getTeam("0000spectator") == null){
            org.bukkit.scoreboard.Team team = this.gameScoreboard.registerNewTeam("0000spectator");
            team.setPrefix("§7");
            team.setCanSeeFriendlyInvisibles(true);
        }

        this.gameScoreboard.getTeam("0000spectator").addEntry(player.getName());
        player.setScoreboard(this.gameScoreboard);
    }
}
