package de.kybu.bedwars.game.scoreboard;

import de.kybu.bedwars.game.BedWarsGame;
import de.kybu.gameframe.scoreboard.GameBoard;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class BedWarsLobbyScoreboard {

    private static BedWarsLobbyScoreboard instance;

    public static BedWarsLobbyScoreboard getInstance() {
        return (instance != null ? instance : (instance = new BedWarsLobbyScoreboard()));
    }

    @Getter private final Scoreboard lobbyScoreboard;

    public BedWarsLobbyScoreboard(){
        this.lobbyScoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
    }

    public void update(BedWarsGame game){
        synchronized (this.lobbyScoreboard){
            GameBoard gameBoard = new GameBoard(this.lobbyScoreboard);

            if(this.lobbyScoreboard.getObjective(DisplaySlot.SIDEBAR) != null)
                this.lobbyScoreboard.getObjective(DisplaySlot.SIDEBAR).unregister();

            gameBoard.display();

            gameBoard.remove(5);
            gameBoard.set(5, "§f");
            gameBoard.remove(4);
            gameBoard.set(4, "§fMap§8:");
            gameBoard.remove(3);
            gameBoard.set(3, "§e" + game.getMap().getMapName());
            gameBoard.remove(2);
            gameBoard.set(2, "§a");
            gameBoard.remove(1);
            gameBoard.set(1, "§fSpieler§8:");
            gameBoard.remove(0);
            gameBoard.set(0, "§a" + game.getBedWarsPlayerMap().size());
        }
    }

    public void setScoreboard(final Player player){

        if(this.lobbyScoreboard.getTeam("0001dev") == null){
            Team team = this.lobbyScoreboard.registerNewTeam("0001dev");
            team.setPrefix("§cDev §7| §c");
        }

        if(this.lobbyScoreboard.getEntryTeam(player.getName()) != null)
            this.lobbyScoreboard.getEntryTeam(player.getName()).removeEntry(player.getName());

        this.lobbyScoreboard.getTeam("0001dev").addEntry(player.getName());
        player.setScoreboard(this.lobbyScoreboard);

    }
}
