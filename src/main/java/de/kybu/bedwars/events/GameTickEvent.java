package de.kybu.bedwars.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameTickEvent extends Event {

    public static HandlerList HANDLERS = new HandlerList();

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public final long startedAt;

    public GameTickEvent(final long startedAt){
        this.startedAt = startedAt;
    }

    public long getStartedAt() {
        return startedAt;
    }
}