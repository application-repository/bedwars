package de.kybu.bedwars.shop;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import de.kybu.achievements.common.IAchievementPlayerProvider;
import de.kybu.achievements.common.model.IAchievementPlayer;
import de.kybu.bedwars.BedWars;
import de.kybu.bedwars.shop.config.BedWarsShopCategoriesConfiguration;
import de.kybu.bedwars.shop.misc.BedWarsShopCategory;
import de.kybu.bedwars.shop.misc.BedWarsShopItem;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.configurations.ConfigurationService;
import de.kybu.gameframe.game.inventory.GameInventory;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;

@Getter
public class BedWarsShop extends GameInventory {

    private final BedWarsShopCategoriesConfiguration categoriesConfiguration;

    public BedWarsShop(){
        super("§8Wähle eine Kategorie", 45, "bw_shop");
        this.categoriesConfiguration = ConfigurationService.getInstance().readConfiguration(BedWarsShopCategoriesConfiguration.class);
        this.setHolder(2);
        this.setHolder(3);
        this.setHolder(4);
        this.fillCategories();
        this.registerClickListener(BedWars.getInstance());
    }

    private void clearArea(int startSlot, int endSlot){
        for(int i = startSlot; i < endSlot +1; i++){
            this.getInventory().remove(i);
        }
    }

    private void fillCategories(){
        this.clearArea(28, 34);
        this.clearArea(37, 43);

        int startIndex = 28;
        for(BedWarsShopCategory category : this.categoriesConfiguration.getCategories().values()){
            this.getInventory().setItem(startIndex, category.buildItem());
            startIndex++;

            if(startIndex == 35)
                startIndex = startIndex + 2;
        }
    }

    @Override
    public void handle(InventoryClickEvent event) {


        if(event.getClickedInventory() == null){
            return;
        }

        if(event.getCurrentItem() == null){
            return;
        }

        if(event.getCurrentItem().getType() == Material.STAINED_GLASS_PANE){
            event.setCancelled(true);
            return;
        }

        if(event.getClickedInventory().getTitle().equals("§8Wähle eine Kategorie") && event.getClickedInventory().getType() == InventoryType.CHEST){
            ((Player) event.getWhoClicked()).updateInventory();
            ItemStack clickedItem = event.getCurrentItem();

            switch (clickedItem.getType()){
                case CLAY_BRICK:
                case IRON_INGOT:
                case GOLD_INGOT:
                    clickedItem = event.getClickedInventory().getItem(event.getSlot() - 9);
                    break;
            }

            event.setCancelled(true);

            if(clickedItem.getType() == Material.STAINED_GLASS_PANE)
                return;

            if(clickedItem.hasItemMeta() && clickedItem.getItemMeta().getDisplayName().endsWith("§f")){
                BedWarsShopMenuHelper.getInstance().openCategory((Player) event.getWhoClicked(), clickedItem.getType(), event.getInventory());
                return;
            }

            if(BedWarsShopMenuHelper.getInstance().getPlayerCategory((Player) event.getWhoClicked()) == null){
                event.getWhoClicked().closeInventory();
                return;
            }

            this.buyItem((Player) event.getWhoClicked(), clickedItem.getType(), clickedItem.getItemMeta().getDisplayName(), BedWarsShopMenuHelper.getInstance().getPlayerCategory((Player) event.getWhoClicked()), event.getClick());
        }
    }

    public void buyItem(final Player player, final Material type, final String displayName, Material categoryItem, ClickType clickType){
        if(type == Material.STAINED_GLASS)
            return;

        BedWarsShopCategoriesConfiguration configuration = BedWarsShopMenuHelper.getInstance().getBedWarsShop().getCategoriesConfiguration();
        BedWarsShopCategory category = configuration.getCategories().get(categoryItem);
        if(category == null)
            return;

        BedWarsShopItem item = category.getCategoryItems().getItems().get(displayName);
        if(item == null)
            return;

        if(item.getNeededType() == Material.GOLD_INGOT){
            Futures.addCallback(IAchievementPlayerProvider.getInstance().getAchievementPlayer(player.getUniqueId()), new FutureCallback<IAchievementPlayer>() {
                @Override
                public void onSuccess(@Nullable IAchievementPlayer iAchievementPlayer) {
                    if(!iAchievementPlayer.hasAchievement(3))
                        iAchievementPlayer.unlockAchievement(3 /* Goldspieler Achievement */);
                }

                @Override
                public void onFailure(Throwable throwable) {
                    throwable.printStackTrace();
                }
            }, GameFrame.getInstance().getExecutorService());
        }

        if(clickType == ClickType.SHIFT_LEFT){

            int times = this.findItemAmount(player.getInventory(), item.getNeededType()) / item.getNeededAmount();
            int neededAmount = item.getNeededAmount() * times;

            if(!player.getInventory().contains(item.getNeededType(), neededAmount)){
                player.playSound(player.getLocation(), Sound.ANVIL_LAND, 3, 1);
                return;
            }

            this.removeItem(player.getInventory(), item.getNeededType(), neededAmount);

            for(int i = 0; i < times; i++){
                player.getInventory().addItem(item.buildMaterial());
            }
            player.playSound(player.getLocation(), Sound.CLICK, 3, 1);
        } else {
            if(!player.getInventory().contains(item.getNeededType(), item.getNeededAmount())){
                player.playSound(player.getLocation(), Sound.ANVIL_LAND, 3, 1);
                return;
            }

            this.removeItem(player.getInventory(), item.getNeededType(), item.getNeededAmount());

            player.getInventory().addItem(item.buildMaterial());
            player.playSound(player.getLocation(), Sound.CLICK, 3, 1);
        }


    }

    private int findItemAmount(final Inventory inventory, Material type){
        int amount = 0;
        for(ItemStack item : inventory.getContents()){
            if(item == null)
                continue;
            if(item.getType() == type)
                amount = amount + item.getAmount();
        }

        if(amount > 64)
            amount = 64;
        return amount;
    }

    private void removeItem(final Inventory inventory, final Material material, final int amount){
        final int first = inventory.first(material);
        if(first != -1){
            final ItemStack itemStack = inventory.getItem(first);
            if(itemStack.getAmount() <= amount)
                inventory.clear(first);
            else {
                itemStack.setAmount(itemStack.getAmount() - amount);
                inventory.setItem(first, itemStack);
            }
        }
    }
}
