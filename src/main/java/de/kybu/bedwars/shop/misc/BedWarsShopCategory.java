package de.kybu.bedwars.shop.misc;

import de.kybu.bedwars.shop.config.BedWarsShopCategoryItemConfiguration;
import de.kybu.gameframe.configurations.ConfigurationService;
import de.kybu.gameframe.item.Item;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

@Getter
public class BedWarsShopCategory {

    private final String categoryName;
    private final String categoryDisplayName;
    private final Material categoryMaterial;
    private transient ItemStack categoryItemStack;
    private transient BedWarsShopCategoryItemConfiguration categoryItems;

    public BedWarsShopCategory(){
        this.categoryName = "Blocks";
        this.categoryDisplayName = "§eBlöcke";
        this.categoryMaterial = Material.SANDSTONE;
    }

    public BedWarsShopCategoryItemConfiguration getCategoryItems() {
        if(this.categoryItems == null)
            this.categoryItems = ConfigurationService.getInstance().readConfiguration(
                    BedWarsShopCategoryItemConfiguration.class,
                    "plugins/GameFrame/modules/BedWars/Shop/" + this.categoryName + "/items.json"
            );
        return categoryItems;
    }

    public ItemStack buildItem(){
        if(this.categoryItems == null)
            this.categoryItemStack = new Item(this.categoryMaterial).setDisplayName(this.categoryDisplayName + "§f").build();

        return this.categoryItemStack;
    }
}
