package de.kybu.bedwars.shop.misc;

import de.kybu.gameframe.item.Item;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Getter
public class BedWarsShopItem {

    private final Material material;
    private final String displayName;
    private final int amountOnBuy;

    private transient ItemStack itemStack;

    private final Material neededType;
    private final int neededAmount;

    private final Map<String, Integer> enchantments;
    private final String[] itemDescription;

    public BedWarsShopItem(){
        this.material = Material.SANDSTONE;
        this.displayName = "§eSandstein";
        this.amountOnBuy = 2;
        this.enchantments = Collections.singletonMap("DAMAGE_ALL", 1);
        this.itemDescription = new String[]{"§7Line1", "§7Line2"};

        this.neededType = Material.CLAY_BRICK;
        this.neededAmount = 1;
    }

    public ItemStack buildMaterial(){
        if(this.itemStack == null){
            if (this.material == null || this.displayName == null || this.itemDescription == null)
                return null;

            Item item = new Item(this.material)
                    .setDisplayName(this.displayName)
                    .addLoreAll(Arrays.asList(itemDescription))
                    .setAmount(amountOnBuy);

            for(Map.Entry<String, Integer> enchantments : this.enchantments.entrySet()) {
                item.addEnchantment(Enchantment.getByName(enchantments.getKey()), enchantments.getValue());
            }

            item.addItemFlag(ItemFlag.HIDE_UNBREAKABLE);
            item.addItemFlag(ItemFlag.HIDE_ENCHANTS);

            this.itemStack = item.build();
        }

        return this.itemStack;
    }
}
