package de.kybu.bedwars.shop.config;

import de.kybu.bedwars.shop.misc.BedWarsShopCategory;
import de.kybu.gameframe.configurations.Configuration;
import lombok.Getter;
import org.bukkit.Material;

import java.util.Collections;
import java.util.Map;

@Getter
@Configuration(path = "plugins/GameFrame/modules/BedWars/Shop/categories.json")
public class BedWarsShopCategoriesConfiguration {

    private final Map<Material, BedWarsShopCategory> categories;

    public BedWarsShopCategoriesConfiguration(){
        this.categories = Collections.singletonMap(Material.SANDSTONE, new BedWarsShopCategory());
    }
}
