package de.kybu.bedwars.shop.config;

import de.kybu.bedwars.shop.misc.BedWarsShopItem;
import lombok.Getter;

import java.util.Collections;
import java.util.Map;

@Getter
public class BedWarsShopCategoryItemConfiguration {

    private final Map<String, BedWarsShopItem> items;

    public BedWarsShopCategoryItemConfiguration(){
        this.items = Collections.singletonMap("§eSandstein", new BedWarsShopItem());
    }
}
