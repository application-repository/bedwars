package de.kybu.bedwars.shop;

import de.kybu.bedwars.shop.misc.BedWarsShopCategory;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BedWarsShopMenuHelper {

    private static BedWarsShopMenuHelper instance;
    private BedWarsShop bedWarsShop;
    private final Map<Material, BedwarsSubShop> categories;
    private final Map<UUID, Material> playerCategory;

    public static BedWarsShopMenuHelper getInstance() {
        return (instance != null ? instance : (instance = new BedWarsShopMenuHelper()));
    }

    public BedWarsShopMenuHelper(){
        this.categories = new HashMap<>();
        this.playerCategory = new HashMap<>();

        GameFrame.getInstance().getExecutorService().execute(() -> {
            this.generateShop();
            this.generateSubShops();
        });
    }

    public void generateShop(){
        this.bedWarsShop = new BedWarsShop();
    }

    public void generateSubShops(){
        for(Map.Entry<Material, BedWarsShopCategory> entry : this.bedWarsShop.getCategoriesConfiguration().getCategories().entrySet()){
            this.categories.put(entry.getKey(), new BedwarsSubShop(entry.getValue(), this.bedWarsShop.getInventory().getContents()));
        }
    }

    public void openShop(final Player player){
        if(this.bedWarsShop == null){
            player.sendMessage(ModuleService.getInstance().getPrefix() + "§cDer Shop lädt, bitte waten...");
            return;
        }

        Inventory inventory = Bukkit.createInventory(null, 45, "§8Wähle eine Kategorie");
        this.openCategory(player, Material.SANDSTONE, inventory);

        player.openInventory(inventory);
    }

    public void openCategory(final Player player, final Material category, final Inventory inventory){
        this.playerCategory.put(player.getUniqueId(), category);
        inventory.setContents(this.categories.get(category).getShopContents());
    }

    public ItemStack[] getCategoryContents(final Material material){
        return this.categories.get(material).getShopContents();
    }

    public Material getPlayerCategory(final Player player){
        return this.playerCategory.get(player.getUniqueId());
    }

    public BedWarsShop getBedWarsShop() {
        return bedWarsShop;
    }
}
