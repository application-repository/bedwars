package de.kybu.bedwars.shop;

import de.kybu.bedwars.shop.misc.BedWarsShopCategory;
import de.kybu.bedwars.shop.misc.BedWarsShopItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Map;

public class BedwarsSubShop {

    private final ItemStack[] shopContents;
    private final BedWarsShopCategory currentCategory;

    public BedwarsSubShop(final BedWarsShopCategory currentCategory, ItemStack[] contents){
        this.currentCategory = currentCategory;
        this.shopContents = this.generateContents(contents);
    }

    private ItemStack[] generateContents(ItemStack[] defaultContents){
        Inventory tempInventory = Bukkit.createInventory(null, 45, "");
        tempInventory.setContents(defaultContents);

        int startIndex = 0;
        for(Map.Entry<String, BedWarsShopItem> entry : this.currentCategory.getCategoryItems().getItems().entrySet()){
            if(entry.getKey().startsWith("!")){
                startIndex++;
                continue;
            }
            tempInventory.setItem(startIndex, entry.getValue().buildMaterial());
            tempInventory.setItem(startIndex + 9, this.generateItem(entry.getValue().getNeededType(), entry.getValue().getNeededAmount()));
            startIndex++;
        }

        return tempInventory.getContents();
    }

    public ItemStack[] getShopContents() {
        return shopContents;
    }

    public BedWarsShopCategory getCurrentCategory() {
        return currentCategory;
    }

    private ItemStack generateItem(final Material material, final int amount){
        ItemStack itemStack = new ItemStack(material, amount);
        ItemMeta itemMeta = itemStack.getItemMeta();
        switch (material){
            case IRON_INGOT:
                itemMeta.setDisplayName("§7Eisen");
                break;
            case CLAY_BRICK:
                itemMeta.setDisplayName("§cBronze");
                break;
            case GOLD_INGOT:
                itemMeta.setDisplayName("§6Gold");
                break;
        }
        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }
}
